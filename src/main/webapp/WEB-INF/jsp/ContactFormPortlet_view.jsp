<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


<portlet:defineObjects />
<portlet:resourceURL var="ajaxResourceUrl"/>

<script type="text/javascript">
    var postData = {
        test1: 1234,
        test2: "hello"
    };

    function <portlet:namespace/>_callAjax() {
        console.log("Execute...");
        var url = '<%=ajaxResourceUrl%>';
        $.ajax({
            type : "POST",
            url : url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            cache:false,
            dataType: "text",
            body: new FormData(postData),
            success : function(data) {
                console.log(data);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.error(errorThrown)
            }
        });
    }

</script>
<button onclick="<portlet:namespace/>_callAjax()" >Call AJAX</button>