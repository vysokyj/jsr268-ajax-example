package com.gitlab.vysokyj;

import javax.portlet.*;
import java.io.IOException;

public class ExamplePortlet extends GenericPortlet {

    private static final String VIEW_JSP = "/WEB-INF/jsp/ContactFormPortlet_view.jsp";
    private static final String EDIT_JSP = "/WEB-INF/jsp/ContactFormPortlet_edit.jsp";
    private static final String HELP_JSP = "/WEB-INF/jsp/ContactFormPortlet_help.jsp";

    @Override
    public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        System.out.println("doView");
        response.setContentType(request.getResponseContentType());
        request.setAttribute("url", request.getPreferences().getValue("url", "/upload"));
        PortletRequestDispatcher dispatcher = getPortletContext().getRequestDispatcher(VIEW_JSP);
        dispatcher.include(request, response);
    }

    @Override
    public void doEdit(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        response.setContentType(request.getResponseContentType());
        PortletRequestDispatcher dispatcher = getPortletContext().getRequestDispatcher(EDIT_JSP);
        dispatcher.include(request, response);
    }

    @Override
    public void doHelp(RenderRequest request, RenderResponse response) throws PortletException, IOException {
        response.setContentType(request.getResponseContentType());
        PortletRequestDispatcher dispatcher = getPortletContext().getRequestDispatcher(HELP_JSP);
        dispatcher.include(request, response);
    }

    protected String viewJSP;


    public void processAction(ActionRequest request, ActionResponse response)
            throws PortletException, IOException {

        System.out.println("########### Inside com.jsr286.portlets.TestPortlet ... processAction() method.....");

        /*
        HttpServletResponse httpResp = PortalUtil.getHttpServletResponse(response);
        httpResp.setContentType("text");
        httpResp.getWriter().print("AJAX Response using Portlet 2.0 Spec - processAction() method .... ");
        httpResp.flushBuffer();
        */
    }

    public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException
    {
        System.out.println("########### Inside com.jsr286.portlets.TestPortlet ... serveResource() method.....");

        response.setContentType("text");
        response.resetBuffer();
        response.getWriter().print("Correct Ajax Resopnse from Portlet 2.0 Spec - serveResource() method .... ");
        response.flushBuffer();
    }

}

